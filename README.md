### Kronbars TeamPage

### Project setup

Перед установкой проекта необходимо создать файл `.env`, в который записать необходимые секретные данные. Пример файла:
```
TWITCH_CLIENT_ID=XXXXXX
TEAM_NAME=itmo
```

В проекте для удобства скрипты описаны в Composer. Запуск производится командой:
```bash
composer install
```

Запуск сервера php:
```bash
composer run --timeout=0 server:run
```

Если Composer'a нет, то все, что нужно сделать - это запустить в корне:

```bash
npm install
npm run build
```

В папке `public` появятся сгенерированные файлы.

Для встраивания на сайт я так понимаю, Вам нужно скопировать разметку из файла `resources/static/index.html` и собранные ассеты из `public/css` и `public/js`.