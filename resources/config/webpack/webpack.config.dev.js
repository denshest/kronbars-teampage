'use strict';

const webpack              = require('webpack');
const merge                = require('webpack-merge');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const helpers              = require('./helpers');
const commonConfig         = require('./webpack.config.common');

const webpackConfig = merge(commonConfig, {
    mode: 'development',
    devtool: 'cheap-module-eval-source-map',
    output: {
        path: helpers.root('public'),
        publicPath: '/',
        filename: 'js/[name].bundle.js',
        hotUpdateMainFilename: '__hmr/[hash].hot-update.json',
        hotUpdateChunkFilename: '__hmr/[id].[hash].hot-update.js'
    },
    devServer: {
        writeToDisk: true,
        compress: true,
        historyApiFallback: true,
        hot: true,
        overlay: true,
        port: 9020,
        stats: {
            normal: true
        }
    },
    plugins: [
        new webpack.EnvironmentPlugin('NODE_ENV'),
        new webpack.HotModuleReplacementPlugin(),
        new FriendlyErrorsPlugin()
    ]
});

module.exports = webpackConfig;
