'use strict';

const HtmlPlugin           = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const Dotenv               = require('dotenv-webpack');
const helpers              = require('./helpers');
const isDev                = process.env.NODE_ENV === 'development';

const webpackConfig = {
    entry: {
        polyfill: '@babel/polyfill',
        index: helpers.root('src', 'index'),
        styles: helpers.root('resources/assets/js', 'styles')
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            'static': helpers.root('resources/static')
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                include: [ helpers.root('resources/assets/js') ],
            },
            {
                test: /\.css$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDev } },
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDev } },
                    { loader: 'sass-loader', options: { sourceMap: isDev } }
                ]
            },
        ]
    },
    plugins: [
        new Dotenv(),
        new HtmlPlugin({ template: helpers.root('resources/static', 'index.html') }),
        new MiniCSSExtractPlugin({
            filename: isDev ? 'css/[name].css' : 'css/[name].[hash].css'
        }),
    ]
};

module.exports = webpackConfig;
