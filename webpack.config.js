'use strict';

const environment = (process.env.NODE_ENV || 'development').trim();

if (environment === 'development') {
    module.exports = require('./resources/config/webpack/webpack.config.dev');
} else {
    module.exports = require('./resources/config/webpack/webpack.config.prod');
}