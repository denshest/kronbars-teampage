const teamList = document.getElementById('list'),
    teamCounter = document.getElementById('team-counter'),
    teamLogo = document.getElementById('logo'),
    teamBanner = document.getElementById('banner');

let embed = undefined;

fetch(`https://api.twitch.tv/kraken/teams/${process.env.TEAM_NAME}`, (data) => {
    const result = JSON.parse(data);

    teamLogo.style.backgroundImage = `url(${result.logo})`;
    teamBanner.style.backgroundImage = `url(${result.banner})`;
    teamCounter.innerHTML = result.users.length;

    let params = '';
    for (let user of result.users) {
        params += `user_id=${user._id}&`;
    }

    fetch(`https://api.twitch.tv/helix/streams?${params}`, (data) => {
        const sortUsers = JSON.parse(data).data;
        let users = result.users;

        for (let user of users) {
            user.viewerCount = 0;
            for (let b of sortUsers) {
                if (user._id === b.user_id) {
                    user.type = b.type;
                    user.viewerCount = b.viewer_count;
                }
            }
        }

        users.sort((a, b) => {
            return b.viewerCount - a.viewerCount;
        });

        for (let user of users) {
            let button = document.createElement('button');
            button.type = 'button';
            button.classList.add('team__list-item');

            let img = document.createElement('img');
            img.src = user.logo;
            img.alt = user.display_name;
            img.classList.add('team__item-image');

            let p = document.createElement('p');
            p.appendChild(document.createTextNode(user.display_name));
            p.classList.add('team__item-name');

            button.append(img, p);

            if (user.type === 'live') {
                let count = document.createElement('div');
                count.classList.add('counter');
                count.appendChild(document.createTextNode(user.viewerCount));

                button.appendChild(count);
            }

            button.addEventListener("click", () => {
                if (user.type === 'live') {
                    changeChannel(user.name);
                    return false;
                }
                window.open(user.url, '_blank');
            });

            teamList.appendChild(button);
        }

        embed = new Twitch.Embed("player", {
            width: '100%',
            height: 400,
            channel: users[0].name,
            theme: 'dark',
            layout: 'video',
            autoplay: true
        });
    });
});

function fetch(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
            callback(xmlHttp.responseText);
    };
    xmlHttp.open("GET", theUrl, true);
    xmlHttp.setRequestHeader('Client-ID', process.env.TWITCH_CLIENT_ID);
    xmlHttp.setRequestHeader('accept', 'application/vnd.twitchtv.v5+json');
    xmlHttp.send(null);
}

function changeChannel(name = '')
{
    let player = embed.getPlayer();
    player.setChannel(name);
}